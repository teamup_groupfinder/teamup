/**
 * @author Marc Weber
 */

var mongoose = require('mongoose')
var Group = require('./models/Group')
var Person = require('./models/Person')
const fs = require('fs');
const path = require('path');
mongoose.set('useFindAndModify', false);

class DB_Util{
    
    uploadAllGroups(dir, fileName){
        
        var groups = {};            
        
        fs.readFile(path.join(__dirname, dir, fileName), 'utf8', (err, data) => {
            if(err) throw err;                
            groups = JSON.parse(data)

            for(var i = 0; i < groups.length; i++){

                var group = new Group(
                    groups[i]
                )

                group.save((err) => {
                    if (err) throw err;
                })
            }
            console.log('File uploaded:' , dir + '/' + fileName);
        })          
    }
        
    downloadAllGroups(dir, fileName){
        
        var query = Group.find({});
    
        // limit our results
        // query.limit(100);
    
        query.exec(function (err, group) {
            if(err) return handleError(err);
            group = JSON.stringify(group)
            fs.appendFile(                
                path.join(__dirname, dir, fileName),
                group,
                err => {
                if (err) throw err;
            }
            )          
            console.log('File written to ', dir + '/' + fileName);
        })
    }

    uploadAllPersons(dir, fileName){
        
        var persons = {};            
        
        fs.readFile(path.join(__dirname, dir, fileName), 'utf8', (err, data) => {
            if(err) throw err;                
            persons = JSON.parse(data)

            for(var i = 0; i < persons.length; i++){

                var person = new Person(
                    persons[i]
                )

                person.save((err) => {
                    if (err) throw err;
                })
            }
            console.log('File uploaded:' , dir + '/' + fileName);
        })          
    }

    downloadAllPersons(dir, fileName){
        
        var query = Person.find({});
    
        // limit our results
        // query.limit(100);
    
        query.exec(function (err, person) {
            if(err) return handleError(err);
            person = JSON.stringify(person)
            fs.appendFile(                
                path.join(__dirname, dir, fileName),
                person,
                err => {
                if (err) throw err;
            }
            )
            console.log('File written to ', dir + '/' + fileName);
        })
    }

    findGroupByID (id, callback){

        console.log('findGroupByID(' , id , ')')

        var query = Group.findOne({'groupID': id});
    
        // limit our results
        // query.limit(100)
        
        query.exec(function (err, group) {
            if(err) return handleError(err)
            callback(group)
        })
    }

    findPersonByID (id, callback){

        console.log('findGroupByID(' , id , ')')

        var query = Person.findOne({'personID': id});
    
        // limit our results
        // query.limit(100)
        
        query.exec(function (err, person) {
            if(err) return handleError(err)
            callback(person)
        })
    }

    getAllGroups (callback){

        var query = Group.find({});
    
        // limit our results
        // query.limit(100);
    
        query.exec(function (err, group) {
            if(err) return handleError(err);
            callback(group)                         
        })
    }

    deleteAllGroups(){

        Group.find()        
        .then(groups =>{

            groups.map((group) => {
                
                console.log('GRUPPE: ' , group)
                Group.findById(group._id)
                .then(group => group.remove())         
                console.log('removed: ' , group._id)
            })
        })

        // Group.findById(_id)
        // .then(group => group.remove())    
    }

    deleteAllPersons(){

        Person.find()        
        .then(persons =>{

            persons.map((person) => {
                
                console.log('PERSON: ' , person)
                Person.findById(person._id)
                .then(person => person.remove())         
                console.log('removed: ' , person._id)
            })
        })

        // Group.findById(_id)
        // .then(group => group.remove())    
    }

    getAllPersons (callback){

        var query = Person.find({});
    
        // limit our results
        // query.limit(100);
    
        query.exec(function (err, person) {
            if(err) return handleError(err);
            callback(person)                         
        })
    }

    updateGroupByID(id, name, value){

        console.log('update group(' , id, name, value , ')')

        var update = {[name] : value}        

        var query = Group.findOneAndUpdate({'groupID': id}, update);
    
        query.exec((err) => {
            if(err) return handleError(err)
        })        
    }

    updatePersonByID(id, name, value){

        console.log('update person(' , id, name, value , ')')

        var update = {[name] : value}        

        var query = Person.findOneAndUpdate({'personID': id}, update);
    
        query.exec((err) => {
            if(err) return handleError(err)
        })        
    }
}
module.exports = DB_Util