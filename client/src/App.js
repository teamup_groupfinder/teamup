/**
 * @author Marcel Singer/aMMokschaf
 * @author Marc Weber
 */

/**
 * App-Component handles the information-flow between the other components.
 * Content.js-Component gets one of the following content-type-prop
 * [mainPage, group, person, subject, profileCreationMask, groupCreationMask, Imprint]
 * 
 */

import React, {Component} from 'react'
import './css/App.css'

import Nav from './components/Nav'
import Logo from './components/Logo'
import Content from './components/Content'
import Sidebar from './components/Sidebar'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      contentType: ' ',
      groupsList: [],
      personsList: [],
      instructorsList: [],
      subjectsList: [],
      currentUser: {},
      currentUsersGroups: [],
      currentGroup: {}
    }

    /* methods for user-input */
    this.setContentType = this.setContentType.bind(this)
    this.listAllGroups = this.listAllGroups.bind(this)
    this.listAllPersons = this.listAllPersons.bind(this)

    /* methods for fetching things */
    this.getGroupsWithMembersToList = this.getGroupsWithMembersToList.bind(this)
    this.getGroupsBySubject = this.getGroupsBySubject.bind(this)
    this.getSingleGroup = this.getSingleGroup.bind(this)
    this.getSinglePerson = this.getSinglePerson.bind(this)
    this.getSingleInstructor = this.getSingleInstructor.bind(this)
    this.getSingleSubject = this.getSingleSubject.bind(this)
    this.setCurrentUserById = this.setCurrentUserById.bind(this)
    this.setCurrentGroupById = this.setCurrentGroupById.bind(this)
    this.doBackup = this.doBackup.bind(this)
    this.doDeleteAllForBackup = this.doDeleteAllForBackup.bind(this)
    /*this.fetchEverything = this.fetchEverything.bind(this)*/
  }

  doBackup(){
    fetch('/backup')
  }

  doDeleteAllForBackup(){
    fetch('/deleteAll')
  }

  componentDidMount() {
    this.setContentType('mainPage')
    
    fetch('/groups')
    .then(res => res.json())
    .then(groups => this.setState({groupsList: groups}, () => console.log('Data fetched', groups)))
    fetch('/persons')
    .then(res => res.json())
    .then(persons => this.setState({personsList: persons}, () => console.log('Data fetched', persons)))
    fetch('/subjects')
    .then(res => res.json())
    .then(subjects => this.setState({subjectsList: subjects}, () => console.log('Data fetched', subjects)))
    fetch('/instructors')
    .then(res => res.json())
    .then(instructors => this.setState({instructorsList: instructors}, () => console.log('Data fetched', instructors)))
    
    //get user with user id from database and set to current user
    this.setCurrentUserById(0);
    this.setCurrentGroupById(8851);
    // this.setCurrentGroupById(123);

    //Template for getting single group into an object
    var testdata
    this.getSingleGroup(7852)
      .then((result) => { 
        testdata = result
        console.log(testdata) 
      })
    console.log('state: ', this.state)
  }

  componentDidUpdate(){
    console.log('app update: state = ', this.state)
    /* SERIOUS performance impacts; seems to fetch several times per second
    fetch('/groups')
    .then(res => res.json())
    .then(groups => this.setState({groupsList: groups}, () => console.log('Data fetched', groups)))
    fetch('/persons')
    .then(res => res.json())
    .then(persons => this.setState({personsList: persons}, () => console.log('Data fetched', persons)))
    fetch('/subjects')
    .then(res => res.json())
    .then(subjects => this.setState({subjectsList: subjects}, () => console.log('Data fetched', subjects)))
    fetch('/instructors')
    .then(res => res.json())
    .then(instructors => this.setState({instructorsList: instructors}, () => console.log('Data fetched', instructors)))
    */
  }

  getSingleGroup(id) {
    return fetch('/groups/' + id)
        .then(response => response.json())
        .then(result => result)
  }

  getSinglePerson(id) {
    return fetch('/persons/' + id)
        .then(response => response.json())
        .then(result => result)
  }

  getSingleInstructor(id) {
    return fetch('/instructors/' + id)
        .then(response => response.json())
        .then(result => result)
  }

  getSingleSubject(id) {
    return fetch('/subjects/' + id)
        .then(response => response.json())
        .then(result => result)
  }
  
  // get User from DB and put into state.currentUser
  setCurrentUserById(id){    
    this.getSinglePerson(id)
      .then(result =>{
        this.setState(currentState => {
          console.log('setCurrentUserResult: ' , result)
          return currentState.currentUser = result;
        })
      })     
  }

  // get Group from DB and put into state.currentGroup
  setCurrentGroupById(id){    
    this.getSingleGroup(id)
      .then(result =>{
        this.setState(currentState => {
          console.log('setCurrentGroup: ' , result)
          return currentState.currentGroup = result;
        })
      })     
  }

  /*
  async fetchEverything() {
    let results = await Promise.all([
      fetch('/groups')
      .then(res => res.json())
      .then(groups => this.setState({groupsList: groups}, () => console.log('1Data fetched', groups))),

      fetch('/persons')
      .then(res => res.json())
      .then(persons => this.setState({personsList: persons}, () => console.log('2Data fetched', persons))),

      fetch('/subjects')
      .then(res => res.json())
      .then(subjects => this.setState({subjectsList: subjects}, () => console.log('3Data fetched', subjects))),

      fetch('/instructors')
      .then(res => res.json())
      .then(instructors => this.setState({instructorsList: instructors}, () => console.log('4Data fetched', instructors)))
    ])

    this.forceUpdate()
  }
*/

  getCurrentUsersGroups(currentUser) {
    //Nonfunctional, to be used to get the groups for sidebar.
    //Needs Array of groups in every person to work, then add logic to .map
    /*var groups

    currentUser.groups.map((group) => {
      return (groups.push())
    }
    return groups*/
  }

  getGroupsWithMembersToList() {
    //Group-Array
    //Group-members array
    //Get groups by id one by one
    //Get members by id one by one
    //return Group-Array
  }

  getGroupsBySubject() {
    /*let jsondata;    
    fetch(url)
      .then(function(u) { return u.json() })
      .then(function(json) { jsondata = json })
     */ 
  }
  
  setContentType = /*async*/ function(contentType) {
    /*if(contentType === 'mainPage') {
      await this.fetchEverything()
    }*/
    console.log('setContentType: ' , contentType)
    this.setState((currentState) => {      
      currentState.contentType = contentType
      return currentState
    })
  }

  listAllGroups = function(persons, groups) {
    console.log('listAllGroups')
    this.setState((currentState) => {
      currentState.personsList = persons
      currentState.groupsList = groups
      return currentState
    })
    this.setContentType('group')
  }

  listAllPersons = function(persons, groups) {
    console.log('handleOnListAllPerson')
    this.setState((currentState) => {
      currentState.personsList = persons
      currentState.groupsList = groups
      return currentState
    })
    this.setContentType('person')
  }

  //Not yet working
  listAllSubjects = function(subjects, instructors) {
    console.log('handleOnListAllSubjects')
    this.setState((currentState) => {
      currentState.subjectsList = subjects
      currentState.instructorsList = instructors
    })
    this.setContentType('subject')
  }

  render() {
    
    // zu Testzwecken
    //var currentUser = {"firstName" : "John", "lastName" : "Doe", "personID" : "7521", "matNumber" : "754123858", "mail" : "doe@yahoo.de", "course" : "Informatik", "lookingForGroup" : "true"}

    return (
      <div id="mainContainer">
        <div id="logo">
          <Logo />
        </div>
        <div id="navigation">
          <Nav
            currentUser={this.state.currentUser}
            setContentType = {this.setContentType} 
            listAllGroups = {() => this.listAllGroups(this.state.personsList, this.state.groupsList)}
            listAllPersons = {() => this.listAllPersons(this.state.personsList, this.state.groupsList)}
          />
        </div>
        <div id="content">
          <Content id="contentComponent" 
            contentType={this.state.contentType} 
            groups={this.state.groupsList} 
            persons={this.state.personsList}
            currentUser={this.state.currentUser}
            setCurrentUserById={this.setCurrentUserById}
            currentGroup={this.state.currentGroup}
            setCurrentGroupById={this.setCurrentGroupById}
            setContentType={this.setContentType}
          />
        </div>
        <div id="sidebar">
          <Sidebar 
            currentUser={this.state.currentUser}
            groups={this.state.currentUsersGroups}
            handleShowGroup={this.handleShowGroup}
          />
        </div>      

        <div>
          <i onClick={this.doDeleteAllForBackup}>D</i>
          <i onClick={this.doBackup}>B</i>
        </div>  

      </div>
    )
  }
}

export default App
