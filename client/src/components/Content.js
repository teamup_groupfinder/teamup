/**
 * @author Marcel Singer/aMMokschaf
 */

/**
 *  Content-Component takes a number of contentTypes.
 *  If contentType is 'person', it needs an array of persons as props.
 *  If contentType is 'group', it needs an array of groups and the matching persons as props.
 *  It then passes those items to either ListOfGroups, ListOfPersons, and so on.
 */

import React from 'react'
import ListOfGroups from './ListOfGroups'
import ListOfPersons from './ListOfPersons'
import Loading from './Loading'
import ProfileCreationMask from './ProfileCreationMask'
import ProfileEditingMask from './ProfileEditingMask'
import GroupCreationMask from './GroupCreationMask'
import GroupEditingMask from './GroupEditingMask'
import Imprint from './Imprint'
import GroupDeletionMask from './GroupDeletionMask';

function Content(props) {
    /*console.log('Content:', props.groups, props.persons, props.contentType)*/
    if(props.contentType === 'group') {
        return <ListOfGroups setContentType={props.setContentType} groups={props.groups} persons={props.persons} currentUser={props.currentUser} />
    }
    else if(props.contentType === 'person') {
       return <ListOfPersons persons={props.persons} />
    }
    else if(props.contentType === 'subject') {
        return <Loading loadString={" Module!"} />
    }
    else if(props.contentType === 'mainPage') {
        return (
            <div>
                <h2 id='welcomeMessage'>Hallo {props.currentUser.firstName} {props.currentUser.lastName}!</h2>
                
            </div>
        )
    }
    else if(props.contentType === 'profileCreationMask') {        
        return <ProfileCreationMask 
            setCurrentUserById={props.setCurrentUserById}
            setContentType={props.setContentType}
        />
    }
    else if(props.contentType === 'profileEditingMask') {        
        return <ProfileEditingMask 
            currentUser={props.currentUser} 
            setCurrentUserById={props.setCurrentUserById}
            setContentType={props.setContentType}
        />
    }
    else if(props.contentType === 'groupCreationMask') {
       // return <Loading loadString={" GroupCreation!"}/>
        return <GroupCreationMask 
            setCurrentGroupById={props.setCurrentGroupById}
            setContentType={props.setContentType}
        />
    }
    else if(props.contentType === 'groupEditingMask') {
        // return <Loading loadString={" GroupCreation!"}/>
         return <GroupEditingMask 
            currentGroup={props.currentGroup} 
            currentUser={props.currentUser}
            setCurrentGroupById={props.setCurrentGroupById}
            setContentType={props.setContentType}
         />
     }
    else if(props.contentType === 'imprint') {
        return <Imprint />
    }
    else if(props.contentType === 'groupDeletionMask'){
        return <GroupDeletionMask
            currentGroup={props.currentGroup}
            setContentType={props.setContentType}
        />
    }
    else {
        return null
    }
}

export default Content;