/**
 * @author Marcel Singer/aMMokschaf
 * @author Marc Weber
 */

/**
 * DashButtonBar is a component to be used as a navigation-item. The user chooses a subject from a list.
 * This subject is passed to the server if the DashButton is pressed
 */

import React, { Component } from 'react'
import Popup from '../components/Popup'

class DashButtonBar extends Component {
    constructor(props) {
        super(props)
        console.log('DashButtonBar props', this.props)
        this.state = {
            subject: 1,
            showPopup: false,
            feedback: []
        }
        this.togglePopup = this.togglePopup.bind(this)
    }
    
    togglePopup() {
        this.setState({
          showPopup: !this.state.showPopup
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()
        console.log('handleSubmit', this.state.subject, this.props.currentUser.personID)

        fetch('/dashButton/' + this.props.currentUser.personID + '/' + this.state.subject)
            .then(res => res.json())
            .then(feedback => this.handleFeedback(feedback.text))
            //.then(feedback => console.log('FEEDBACK: ', feedback.text))  

        this.props.setContentType('mainPage')     
    }

    handleFeedback = (feedback) => {
        console.log('handleFeedback', feedback)
        this.setState({
            feedback: feedback
          })
        this.togglePopup()
    }

    handleInputChange = (event) =>{
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        return(
            <div id="dashButtonBar">
                <form onSubmit={this.handleSubmit} method="GET" className="form-inline">
                    <div className="form-group">
                        <div className='ProfileInputFields'>
                            <select name="subject" onChange={this.handleInputChange} value={this.state.subject}>
                                <option value="1">Mathematik 1</option>
                                <option value="2">Mathematik 2</option>
                                <option value="3">Mathematik 3</option>
                                <option value="4">Softwarepraktikum</option>
                                <option value="5">Webtechnologien 2</option>
                            </select>
                        </div>
                        <div className='ProfileInputFields'>
                            <input type="submit" value="DASHBUTTON" ></input>
                        </div>  
                    </div>
                </form>
                {this.state.showPopup ? 
                    <Popup
                        reasons={this.state.feedback}
                        closePopup={this.togglePopup.bind(this)}
                    />
                    : null
                }
            </div>
        )
    }
}

export default DashButtonBar