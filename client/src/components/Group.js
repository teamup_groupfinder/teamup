/**
 * @author Marcel Singer/aMMokschaf
 */

/**
 * Group takes props to render a Group-Component.
 * On mounting, it checks if the group is full and if the logged-in user is a member of this group.
 * It currently has client-side methods to join or leave groups; these should be moved server-side asap.
 * 
 */

import React, { Component } from 'react'
import Person from './Person'
import '../css/Group.css'

class Group extends Component {
    constructor(props) {
        super(props)
        this.state = {
            thisGroup: this.props.group,
            thisGroupsMembers: this.props.members,
            expanded: props.expanded,
            isGroupMember: false,
            groupFull: false
        }
        this.expandOrCollapse = this.expandOrCollapse.bind(this)
        this.joinGroup = this.joinGroup.bind(this)
        this.leaveGroup = this.leaveGroup.bind(this)
        this.isCurrentUserGroupMember = this.isCurrentUserGroupMember.bind(this)
        this.isGroupFull = this.isGroupFull.bind(this)
    }

    componentDidMount() { 
        this.isGroupFull()
        this.isCurrentUserGroupMember(this.props.group, this.props.currentUser)
    }

    /*
    This negates state.expanded when called. Called via the button in render().
    */
    expandOrCollapse() {
        this.setState((currentState) => {
            return currentState.expanded = !currentState.expanded
        })
    }

    /*
    If this method is called, it adds the current-user's memberID to this group's memberIDs-Array.
    It then passes this group as a whole to the update-route for groups.
    */
    
    joinGroup = (event) => {
        event.preventDefault()
        if(this.isGroupFull()) { 
            console.log('Group is full.')
        } else {
            //Doesn't work
            //Better: Save data to an object, pass that object to fetch.post, reload the data via fetch.get
            /*this.setState(currentState => {
                currentState.thisGroup.memberIDs.push(String(this.props.currentUser.personID))
                currentState.thisGroup.numberOfMembers = currentState.thisGroup.memberIDs.length
                return currentState
            })*/

            //Works, but throws warning (do not mutate state directly)
            this.state.thisGroup.memberIDs.push(String(this.props.currentUser.personID))
            this.state.thisGroup.numberOfMembers = this.state.thisGroup.memberIDs.length

            //AlreadyTried for Demo
            this.state.thisGroup.alreadyTried = true
                                    
            const headers = new Headers()
            headers.append('Content-Type', 'application/json')
                        
            const options = {
                method: 'POST',
                headers,
                body: JSON.stringify(this.state.thisGroup)
            };
                                    
            fetch(
                '/groups/' + this.state.thisGroup._id + '/update/', options
            ).then(() => {
                this.props.setContentType('mainPage')
                this.props.setContentType('group')
            }).then().catch(e => console.log("error: ", e))
        }
        this.forceUpdate()
    }

    /*
    If this method is called, it removes the current-user's memberID from this group's memberIDs-Array.
    It then passes this group as a whole to the update-route for groups.
    */
    leaveGroup = (event) => {
        event.preventDefault()
        
        var i
        for(i = 0; i < this.state.thisGroup.memberIDs.length; i++) {
            if(Number(this.state.thisGroup.memberIDs[i]) === Number(this.props.currentUser.personID)) {
                //Doesn't work
                //Save data to an object, pass that object to fetch.post, reload the data via fetch.get
                /*this.setState(currentState => {
                    currentState.thisGroup.memberIDs.pop(this.props.currentUser.personID)
                    currentState.thisGroup.numberOfMembers = currentState.thisGroup.memberIDs.length
                    return currentState
                })*/

                //Works, but throws warning (do not mutate state directly)
                this.state.thisGroup.memberIDs.pop(this.props.currentUser.personID)
                this.state.thisGroup.numberOfMembers = this.state.thisGroup.memberIDs.length  
            }
        }
                                
        const headers = new Headers()
        headers.append('Content-Type', 'application/json')
                
        const options = {
            method: 'POST',
            headers,
            body: JSON.stringify(this.state.thisGroup)
        };
                                
        fetch(
            '/groups/' + this.state.thisGroup._id + '/update/', options
        ).then().catch(e => console.log("error: ", e))
        .then(() => {
            this.props.setContentType('mainPage')
            this.props.setContentType('group')
        })
        
        this.forceUpdate()
    }

    /*
    This checks if the currently logged-in user is a member of this specific group.
    */
    isCurrentUserGroupMember(group, currentUser) {
        var i
        for(i = 0; i < group.memberIDs.length; i++) {
            if(Number(group.memberIDs[i]) === Number(currentUser.personID)) {
                this.setState(currentState => {
                    currentState.isGroupMember = true
                    return currentState
                })
            }
        } 
    }

    /*
    isGroupFull() checks if there are as many members in the group as can be or not,
    updates state.groupFull accordingly
    */
    isGroupFull() {
        if(this.props.group.memberIDs.length === Number(this.props.group.maxMembers)) {
            this.setState(currentState => {
                currentState.groupFull = true
                return currentState
            })
        }
        else {
            this.setState(currentState => {
                currentState.groupFull = false
                return currentState
            })
        }
    }

    /*
    render() checks if expanded is true, then returns small or big Group-Component
    */
    render() {
        /* Expanded Group */
        if(this.state.expanded) {
            return (
                <div className='divGroup'>
                    <div id='line' />
                    <div className='groupInfos' onClick={this.expandOrCollapse}>
                        <h3>{this.props.group.groupName}</h3>
                        <h5>Gruppe #{this.props.group.groupID}</h5>
                        <p>Mitglieder: {this.props.group.numberOfMembers}/{this.props.group.maxMembers}</p>
                        <p>{this.props.group.description}</p>
                    </div>
                    <div className='groupMembers' onClick={this.expandOrCollapse}>
                        {this.state.thisGroupsMembers.map((member) => (
                            <div key={member.personID}><Person {...member} icon={true} /></div>
                        ))}
                    </div>
                    <div className='groupButtonJoinLeave'>
                        {
                            this.state.groupFull
                                ? (
                                    this.state.isGroupMember 
                                    ? <button type="button" id="join-leave" onClick={this.leaveGroup}>Verlassen</button>
                                    : <button type="button" id="join-leave" onClick={this.joinGroup} disabled>Beitreten</button>
                                )
                                : (
                                    this.state.isGroupMember 
                                    ? <button type="button" id="join-leave" onClick={this.leaveGroup}>Verlassen</button>
                                    : <button type="button" id="join-leave" onClick={this.joinGroup}>Beitreten</button>
                                )
                        }  
                    </div>
                    <div className='groupButtonExpandCollapse' onClick={this.expandOrCollapse}>
                         <button type="button" id="expand-collapse">-</button>  
                    </div>
                    <div className='space' onClick={this.expandOrCollapse} />
                </div>
            )
        }
        else {
        /* Collapsed Group */
            return (
                <div className='divGroup' onClick={this.expandOrCollapse}>
                    <div id='line' />
                    <div className='groupInfos'>
                        <h3>{this.props.group.groupName}</h3>
                        <h5>Gruppe #{this.props.group.groupID}</h5>
                    </div>
                    <div className='groupMembers'>
                        <h3>Mitglieder: {this.props.group.numberOfMembers}/{this.props.group.maxMembers}</h3>
                    </div>
                    <div className='groupButtonExpandCollapse'>
                        <button type="button" id="expand-collapse">+</button>
                    </div>
                </div>
            )
        }
    }
}

export default Group