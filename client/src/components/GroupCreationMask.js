/**
 * @author: Jonathan Krüger
 * @author: Marc Weber
 * @author: Marcel Singer
 */

import React, {Component} from 'react'

class GroupCreationMask extends Component {

    constructor(props){
        super(props);

        this.state = {
            groupName: '',
            groupID: '',
            maxMembers: '',
            numberOfMembers: '1',
            description: '',
            instructor: '',
            memberIDs: ['123'] // fuer Praesentation
        }

        this.groupNameReference = React.createRef();
    }

    componentDidMount(){
        this.groupNameReference.current.focus();
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log('Submitted State: ' , this.state);
        
        // check for duplicate group in the database 
        fetch('/groups/' + this.state.groupID)
        .then(response => response.json())
        .then(result => {
            
            console.log('RESULT: ', result);
            
            // no duplicate, continue creating profile                
            if(result === null){
                console.log('creating group!');

                // save state in data
                const data = this.state;
                
                // headers is needed for database to accept POST request
                const headers = new Headers();
                headers.append('Content-Type', 'application/json');
                
                // wrap method, headers and body
                const options = {
                    method: 'POST',
                    headers,
                    // create body with state (data)
                    body: JSON.stringify(data),
                };
                
                // fetch the 'group' route with POST request
                fetch(
                    '/groups', options
                    ).then(()=>{
                        this.props.setCurrentGroupById(this.state.groupID)
                        this.props.setContentType('mainPage')
                    })   
                
            }else{
                // duplicate found, abort profile creation
                console.log('Gruppe schon vorhanden!')                
            }
        })
        this.props.setContentType('mainPage')
    }
        
    handleInputChange = (event) =>{
        event.preventDefault();
        this.setState(
            {
             [event.target.name]: event.target.value
            }
        )        
    }
        
    render(){
        return(
            <div className="GroupCreationMask">
                <div className='maskHeaders'><h3>Gruppe erstellen</h3></div>
                <form onSubmit={this.handleSubmit}>
                    <div className='GroupCreationField'>
                        Gruppenname:
                        <input type="text" name='groupName' required onChange={this.handleInputChange} ref={this.groupNameReference} value={this.state.groupName}/>
                        ID:
                        <input type="text" name="groupID" required onChange={this.handleInputChange} value={this.state.groupID}/>
                        maxMembers:
                        <input type="text" name="maxMembers" required onChange={this.handleInputChange} value={this.state.maxMembers}/>
                        description:
                        <input type="text" name="description" required onChange={this.handleInputChange} value={this.state.description}/>
                        <div className='ProfileInputFields'>
                            <select name="subjectID" onChange={this.handleInputChange} value={this.state.subject}>
                                <option value="1">Mathematik 1</option>
                                <option value="2">Mathematik 2</option>
                                <option value="3">Mathematik 3</option>
                                <option value="4">Softwarepraktikum</option>
                                <option value="5">Webtechnologien 2</option>
                            </select>
                        </div>
                    </div>                   
                    <div className='GroupCreationField'>
                        <input type="submit"value="Gruppe speichern"></input>
                    </div>  
                </form>  
            </div>
        )
    }
}

export default GroupCreationMask
