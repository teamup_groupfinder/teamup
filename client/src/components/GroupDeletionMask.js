/**
 * @author: Jonathan Krueger
 */

import React, { Component } from 'react'



class GroupDeletionMask extends Component {
    constructor(props) {
        super(props)

        this.state = {
            currentGroup: {},
            groupID: ''
        }
    }
    // Fetches single group form DB
    getSingleGroup = async (id) => {
        return await fetch('/groups/' + id)
            .then(response => response.json())
            .then(result => result)
    }

    //updates this.state.currentGroup with result of getSingleGroup()
    getGroup = async (id) => {
        await this.getSingleGroup(id)
            .then(result => {
                this.setState(currentState => {
                    console.log('setGroup: ', result)
                    return currentState.currentGroup = result;


                })

            })

    }
    // Deletes single Group from DB
    deleteSingleGroup = async (id) => {
        return await fetch('/groups/' + id + '/delete', { method: 'DELETE' })
            .then(response => response.json())
            .then(result => result)
            .then(this.props.setContentType('mainPage'))
    }

    //what happens when the user clicks "Delete Group"
    handleSubmit = async (event) => {
        event.preventDefault()
        await this.getGroup(this.state.groupID)
        this.deleteSingleGroup(this.state.currentGroup._id)

    }


    // update state with input
    handleInputChange = (event) => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        return (
            <div className='GroupDeletionMask'>
                <div className='maskHeaders'><h3>Gruppe l&ouml;schen</h3></div>
                <form onSubmit={this.handleSubmit}>

                    <div className='GroupIDInput'>
                        Enter Group ID:

                        <input type="text" name="groupID" onChange={this.handleInputChange} value={this.state.groupID} />
                        <input type="submit" value="Delete Group" />

                    </div>
                </form>
            </div>
        )
    }


}




export default GroupDeletionMask
