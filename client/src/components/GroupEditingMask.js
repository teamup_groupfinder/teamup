/**
 * @author: Marc Weber
 */

import React, {Component} from 'react'
import ListOfPersons from './ListOfPersons'

class GroupEditingMask extends Component {

    constructor(props){
        super(props);                
        // this.state = props.currentGroup                
        this.state = {
            currentGroup: props.currentGroup,
            persons: []
        }
        this.groupNameReference = React.createRef();
    }

    componentDidMount(){
        this.groupNameReference.current.focus();
        // console.log('groupEditingMask MOUNT: ', this.state)
        this.createPersonList()
    }

    componentDidUpdate(){    
        console.log('groupEditingMask UPDATE:', this.state)
    }

    handleSubmit = (event) => {
        event.preventDefault();

        // save state in data
        const data = this.state.currentGroup;

        console.log('DATA: ', data )

        const groupID = data._id;
            
        // headers is needed for database to accept POST request
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        // wrap method, headers and body
        const options = {
            method: 'POST',
            headers,
            // create body with state (data)
            body: JSON.stringify(data),
        };
            
        // fetch the 'groups' route with POST request and update group
        fetch(
            '/groups/' + groupID + '/update', options
        ).then(()=>{
            this.props.setCurrentGroupById(data.groupID)
            this.props.setContentType('mainPage')
        })        
    }
    
    // update state with input
    handleInputChange = (event) =>{
        event.preventDefault();              
        const target = event.target.name
        const value = event.target.value        
        
        this.setState((currentState) => {
            currentState.currentGroup[target] = value
            return currentState
        })
    }

    getSinglePerson(id) {
        return fetch('/persons/' + id)
            .then(response => response.json())
            .then(result => result)
    }

    createPersonList(){

        const group = this.state.currentGroup

        group.memberIDs.map((id) => {
            return this.getSinglePerson(id)
            .then((person) => this.setState((currentState)=>currentState.persons.push(person)))
        })
        console.log('done')
    }

    render() {
        return (            
            <div id='GroupEditingMask'>
                <div id='GroupEditingMaskHeader'>
                    <div className='maskHeaders'><h3>Gruppe bearbeiten</h3></div>
                    <div className='mitglieder'>Mitglieder: {this.state.currentGroup.memberIDs.length} / {this.state.currentGroup.maxMembers}</div>
                    <ListOfPersons persons={this.state.persons} />
                </div>
                
                <form onSubmit={this.handleSubmit}>
                    <div className='GroupEditingField'>                        
                        Gruppenname:
                        <input name='groupName' onChange={this.handleInputChange} ref={this.groupNameReference} value={this.state.currentGroup.groupName}/>
                        maxMembers:
                        <input type="text" name="maxMembers" onChange={this.handleInputChange} ref={this.groupNameReference} value={this.state.currentGroup.maxMembers}/>
                        description:
                        <textarea name="description" onChange={this.handleInputChange} ref={this.groupNameReference} value={this.state.currentGroup.description}/>
                        Instructor:
                        <input type="text" name="instructor" onChange={this.handleInputChange} ref={this.groupNameReference} value={this.state.currentGroup.instructor}/>
                    </div>                   
                    <div className='GroupEditingField'>
                        <input type="submit"value="Gruppe speichern" ></input>
                    </div>  
                </form>                
            </div>
        )
    }
}

export default GroupEditingMask
