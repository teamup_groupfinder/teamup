/**
 * @author: Marcel Singer/aMMokschaf
 * @author: Nils 'ng0' Gillmann
 */

/**
 *  Imprint shows the 'Impressum' in the content-component
 */

import React from 'react'
import '../css/Imprint.css'

function Imprint() {
    return(
      <div id="imprint">
        <h2>Impressum</h2>
        <h3>Anbieter</h3>
	<p> 
        Grottenolm Design und andere Fehler GmbH<br />
        Musterstr. 23<br />
        10115 Berlin<br /><br />
	</p>
        <h3>Kontakt:</h3>
	<p> 
        Telefon: <a href="tel:01189998819991197253">0118 999 881 999 119 725 3</a><br />
	{/* rfc2086, section 2.3 but no modern webbrowser supports this ;) */}
        Telefax: <a href="fax:01189998819991197254">0118 999 881 999 119 725 4</a><br />
        E-Mail: <a href="mailto:mail@mustermann.de">mail@mustermann.de</a><br />
        Website: <a href="gopher://www.mustermann.de">www.mustermann.de</a><br /><br />
        </p>
        <h3>Bei redaktionellen Inhalten:</h3>
	<p>
        Verantwortlich nach &#xA7; 55 Abs.2 RStV<br />
        Moritz Impressiv<br />
        Musterstr. 23<br />
        10115 Berlin
        </p>
      </div>
    )
}

export default Imprint;
