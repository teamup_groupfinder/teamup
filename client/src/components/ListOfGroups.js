/**
 * @author Marcel Singer/aMMokschaf
 */

/**
 * ListOfGroups gets an array of groups and an array of persons.
 * All group-objects are passed to a Group-Component along with the persons-array.
 * Group uses lookForGroupMembers to display all members, if Group is state.expanded.
 */

import React, { Component } from 'react'
import Group from './Group'

function lookForGroupMembers(group, persons) {
    console.log("lookforgroupmembers Anfang", persons)
    var members = []
    members = persons.filter((person) => {
        var i
        for(i = 0; i < group.memberIDs.length; i++) {
            //console.log(Number(group.memberIDs[i]) === person.personID)
            if(Number(group.memberIDs[i]) === person.personID) {  
                return person
            }
        }
       return null
    })
    console.log("lookforgroupmembers Ende", members)
    return members
}

class ListOfGroups extends Component {
    constructor(props) {
        super(props)
        this.state = {
            groupsList: [],
            personsList: []
        }
    }

    async componentWillMount() {
        let results = await Promise.all([
            fetch('/groups')
            .then(res => res.json())
            .then(groups => this.setState({groupsList: groups}, () => console.log('Data fetched', groups))),
            fetch('/persons')
            .then(res => res.json())
            .then(persons => this.setState({personsList: persons}, () => console.log('Data fetched', persons))) 
        ])
        console.log(results)
        console.log(this.state)
    }

    async componentDidMount() {
        await this.componentWillMount() 
    }

    render () {
        //Check if any of the needed fetches are not done yet. If persons or groups are length = 0, return nothing.
        //Rerender if data is fetched.
        if(this.state.personsList.length === 0 || this.state.groupsList.length === 0) {
            console.log('No Data')
            return <div />
        }
        var members = []
        var wrapper = []
        var currentUser = this.props.currentUser
        return (  
            <div className="listOfGroups">
                <ul>
                    {this.state.groupsList.map((group) => {
                        members = lookForGroupMembers(group, this.state.personsList);
                        wrapper = {group, members, currentUser}
                        return (<li key={group.groupID}><Group {...wrapper} expanded={false} setContentType={this.props.setContentType} /></li>)
                    })}
                </ul>
            </div>
        ) 
    } 
}

export default ListOfGroups