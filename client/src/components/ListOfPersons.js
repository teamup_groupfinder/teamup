/**
 * @author Marcel Singer/aMMokschaf
 */

/**
 * ListOfPersons gets an array of persons to pass to a Person-Component, which displays it.
 */

import React, { Component } from 'react'
import Person from './Person'

class ListOfPersons extends Component {
    /*constructor(props) {
        super(props)
    }*/
    render() {
        return (
            <div className="ListOfPersons">
                <ul>
                    {this.props.persons.map((person) => (
                        //TODO Change expanded to only be true if onClick
                        <li key={person.personID}><Person {...person} expanded={false} /></li>
                    ))}
                </ul>
            </div>
        )
    }
}

export default ListOfPersons