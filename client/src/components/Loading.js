/**
 * @author Marcel Singer/aMMokschaf
 */

/**
 * Loading-Component with animation via css-Keyframes. Shown when the app is waiting for the server.
 */

import React from 'react'
import '../css/Loading.css'
import loading from '../images/loading-cog.png'

function Loading(props) {
    return (
        <div id="loading">
            <h1 style= {{textAlign: 'center'}}>Loading {props.loadString}</h1>
            <img className='loading-image' src={loading} alt='Loading-Cog'/>
        </div>
    )
}

export default Loading