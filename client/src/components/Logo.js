/**
 * @author Marcel Singer/aMMokschaf
 */

import React from 'react'
/*import logoImage from '../images/teamUp-logo.jpg'*/
import logoImage from '../images/teamUp-logo_new_transparent.png'

function Logo() {
    return (
      <div>
        <img id='logo' src={logoImage} alt='TeamUp-Logo'/>
      </div>
    )
}

export default Logo