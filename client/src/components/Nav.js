/**
 * @author Marcel Singer/aMMokschaf
 */

/**
 * Nav handles user-input and tells App-Component which information to pass to Content-Component.
 * It sets the contentType to enable Content.js to render the proper component.
 */

import React from 'react'
import SearchBar from '../components/SearchBar'
import DashButtonBar from '../components/DashButtonBar'
import '../css/Nav.css'

function Nav(props) {
    return (
      <div>
        <div className="Navigation">
          <nav className="nav" id="navleft">
            <ul>
              <li onClick={props.listAllGroups}>Alle Gruppen</li>
              <li onClick={props.listAllPersons}>Alle Personen</li>
              <li onClick={props.listAllSubjects}>Alle Module</li>
            </ul> 
          </nav>
          <nav className="nav" id="navcenter">
            <ul>
              <li onClick={() => props.setContentType('profileCreationMask')}>Profil erstellen</li>
              <li onClick={() => props.setContentType('profileEditingMask')}>Profil bearbeiten</li>
              <li onClick={() => props.setContentType('groupCreationMask')}>Gruppe erstellen</li>
              <li onClick={() => props.setContentType('groupEditingMask')}>Gruppe bearbeiten</li>
              <li onClick={() => props.setContentType('groupDeletionMask')}>Gruppe löschen</li>
            </ul>
          </nav>
          <nav className="nav" id="navright">
            <ul>
              <li onClick={() => props.setContentType('mainPage')}>Hauptseite</li>
              <li onClick={() => props.setContentType('imprint')}>Impressum</li>
            </ul>
          </nav>
          {/*<div className="search">
            <SearchBar />
          </div>*/}
          <div className="dashButtonBar">
            <DashButtonBar currentUser={props.currentUser} setContentType={props.setContentType} />
          </div>
        </div>
      </div>
    )
}

export default Nav
