/**
 * @author Marcel Singer/aMMokschaf
 */

/**
 * Person-Component gets props to display. Person can either be 'icon' (for use inside of Group-Component),
 * or can be expanded/collapsed to show more/less information.
 * 
 * TODO: Fill in required props.
 */

import React, { Component } from 'react'
import '../css/Person.css';

class Person extends Component {
    constructor(props) {
        super(props)
        this.state = {
            icon: props.icon, //icon overrides expanded
            expanded: props.expanded
        }
        this.expandOrCollapse = this.expandOrCollapse.bind(this)
    }

    expandOrCollapse() {
        this.setState((currentState) => {
            return currentState.expanded = !currentState.expanded
        })
    }

    render() {
        if(this.state.icon) {
            return (
                <div className='personIcon'>
                    <h3>{this.props.firstName} {this.props.lastName}</h3>
                    <h3>User# {this.props.personID}</h3>
                </div>
            )
        }
        if(this.state.expanded) {
            return (
                <div className='divPerson' onClick={this.expandOrCollapse}>
                    <div id='line' />
                    <div className="personLeft">
                        <h3>{this.props.firstName} {this.props.lastName}</h3>
                        <h5>User# {this.props.personID}</h5>
                        <h5>Matrikelnummer: {this.props.matNumber}</h5>
                        <h5>{this.props.mail}</h5>
                    </div>
                    <div className="personRight">
                        <h3>Studiengang: {this.props.course}</h3>
                    </div>
                    <div className='personButtonExpandCollapse'>
                         <button type="button" id="expand-collapse">-</button>  
                    </div>
                </div>
            )
        }
        else {
            return (
                <div className='divPerson' onClick={this.expandOrCollapse}>
                    <div id='line' />
                    <div className="personName">
                        <h3>{this.props.firstName} {this.props.lastName}</h3>
                    </div>
                    <div className="personCourse">
                        <h3>Studiengang: {this.props.course}</h3>
                    </div>
                    <div className='personButtonExpandCollapse'>
                         <button type="button" id="expand-collapse" >+</button>  
                    </div>
                </div>
            )
        }
    }
}

export default Person