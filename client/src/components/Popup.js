/**
 * @author Marcel Singer/aMMokschaf
 */

 //TODO: Let component map through reasons Array instead of printing reasons 0-3

 /*
Popup displays a dark div with a lighter div in the middle. The lighter div displays the passed
Array. The first Element of the Array is the groupName.
 */
import React, { Component } from 'react'
import '../css/Popup.css'

class Popup extends Component {
  render() {
    return (
      <div className='background'>
        <div className='window'>
          <h3 id='groupName'>Du wurdest folgender Gruppe zugewiesen: <br /> {this.props.reasons[0]}</h3>
          <div id='popupContent'>
            <h4>Gründe:</h4>
            <h5>{this.props.reasons[1]}</h5>
            <h5>{this.props.reasons[2]}</h5>
            <h5>{this.props.reasons[3]}</h5>
            <h5>{this.props.reasons[4]}</h5>
          </div>      
          <button id="closePopupButton" onClick={this.props.closePopup}>Okay</button>
        </div>
      </div>
    );
  }
}

export default Popup