/**
  * @author: Marc Weber
  */

import React, {Component} from 'react'

class ProfileCreationMask extends Component {

    constructor(props){
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            personID: '',
            matNumber: '',
            mail: '',
            course: 'Informatik'
        }

        this.firstNameReference = React.createRef();
    }

    componentDidMount(){
        this.firstNameReference.current.focus();
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log('Submitted State: ' , this.state);

        // check for duplicate profile in the database 
        fetch('/persons/' + this.state.personID)
        .then(response => response.json())
        .then(result => {

            console.log('RESULT: ', result);

            // no duplicate, continue creating profile                
            if(result === null){
                
                console.log('creating profile!');

                // save state in data
                const data = this.state;
        
                // headers is needed for database to accept POST request
                const headers = new Headers();
                headers.append('Content-Type', 'application/json');
        
                // wrap method, headers and body
                const options = {
                    method: 'POST',
                    headers,
                    // create body with state (data)
                    body: JSON.stringify(data),
                };
        
                // fetch the 'persons' route with POST request
                fetch(
                    '/persons', options
                ).then(()=>{
                    this.props.setCurrentUserById(this.state.personID)
                    this.props.setContentType('mainPage')
                })   

            // duplicate found, abort profile creation
            }else{
                console.log('Profil schon vorhanden!')
            }
        })
        this.props.setContentType('mainPage')
    }
    
    handleInputChange = (event) =>{
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {        
        return (
            <div id='ProfileCreationMask'>

                <div className='maskHeaders'>
                <h3>Profil erstellen</h3>
                </div>

                <form onSubmit={this.handleSubmit}>
                    <div className='ProfileInputFields'>
                        Vorname:
                        <input type="text" name="firstName" onChange={this.handleInputChange} ref={this.firstNameReference} value={this.state.firstName}/>
                    </div>
                    <div className='ProfileInputFields'>
                        Nachname:
                        <input type="text" name="lastName" onChange={this.handleInputChange} value={this.state.lastName} />
                    </div>
                    <div className='ProfileInputFields'>
                        E-Mail:
                        <input type="text" name="mail" onChange={this.handleInputChange} value={this.state.mail}/>
                    </div>
                    <div className='ProfileInputFields'>
                        ID:
                        <input type="text" name="personID" onChange={this.handleInputChange} value={this.state.personID}/>
                    </div>
                    <div className='ProfileInputFields'>
                        Matrikelnummer:
                        <input type="text" name="matNumber" onChange={this.handleInputChange} value={this.state.matNumber}/>
                    </div>
                    <div className='ProfileInputFields'>
                        Studiengang:
                        <select name="course" onChange={this.handleInputChange} value={this.state.course}>
                        <option value="Informatik">Informatik</option>
                        <option value="Elektrotechnik">Elektrotechnik</option>
                        <option value="Mechatronik">Mechatronik</option>
                        </select>
                    </div>                    
                    <div className='ProfileInputFields'>
                        <input type="submit" value="Profil erstellen" ></input>
                    </div>          
                </form>                
            </div>
        )
    }
}

export default ProfileCreationMask
