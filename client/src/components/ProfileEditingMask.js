/**
  * @author: Marc Weber
  * @author: Jonathan Krüger
  */

import React, {Component} from 'react'

class ProfileEditingMask extends Component {

    constructor(props){
        super(props);        
        this.state = props.currentUser;
        this.firstNameReference = React.createRef();
    }

    componentDidMount(){
        this.firstNameReference.current.focus();        
    }

    componentDidUpdate(){                
    }

    handleSubmit = (event) => {
        event.preventDefault();

        // save state in data
        const data = this.state;
        console.log('profileupdate', this.state)

        const userID = this.state._id;
            
        // headers is needed for database to accept POST request
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        // wrap method, headers and body
        const options = {
            method: 'POST',
            headers,
            // create body with state (data)
            body: JSON.stringify(data),
        };
            
        // fetch the 'persons' route with POST request and update user
        fetch(
            '/persons/' + userID + '/update', options
        ).then(()=>{
            this.props.setCurrentUserById(this.state.personID)
            this.props.setContentType('mainPage')
        })        
    }
    
    // update state with input
    handleInputChange = (event) =>{
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        return (            
            <div id='ProfileEditingMask'>
                
                <div id='maskHeaders'>
                <h3>Profil bearbeiten</h3>
                </div>

                <form onSubmit={this.handleSubmit}>
                    <div className='ProfileEditingField'>
                        Vorname:
                        <input type="text" name="firstName" onChange={this.handleInputChange} ref={this.firstNameReference} value={this.state.firstName}/>
                    </div>
                    <div className='ProfileEditingField'>
                        Nachname:
                        <input type="text" name="lastName" onChange={this.handleInputChange} value={this.state.lastName} />
                    </div>
                    <div className='ProfileEditingField'>
                        E-Mail:
                        <input type="text" name="mail" onChange={this.handleInputChange} value={this.state.mail}/>
                    </div>
                    <div className='ProfileEditingField'>
                        ID:
                        <input type="text" name="personID" onChange={this.handleInputChange} value={this.state.personID}/>
                    </div>
                    <div className='ProfileEditingField'>
                        Matrikelnummer:
                        <input type="text" name="matNumber" onChange={this.handleInputChange} value={this.state.matNumber}/>
                    </div>
                    <div className='ProfileEditingField'>
                        Studiengang:
                        <select name="course" onChange={this.handleInputChange} value={this.state.course}>
                        <option value="Informatik">Informatik</option>
                        <option value="Elektrotechnik">Elektrotechnik</option>
                        <option value="Mechatronik">Mechatronik</option>
                        </select>
                    </div>                    
                    <div className='ProfileEditingField'>
                        <input type="submit"value="Profil speichern" ></input>
                    </div>          
                </form>                
            </div>
        )
    }
}

export default ProfileEditingMask
