/**
 * @author Marcel Singer/aMMokschaf
 */

/**
 * SearchBar is a component to be used as a navigation-item. It uses the input-String to search for groups in the database.
 * 
 * //TODO: Is unfunctional as of now, implement fuzzy search if possible.
 * 
 */

import React, { Component } from 'react'
import '../css/SearchBar.css'
/* import searchIcon from '../images/search_icon_max.png' */

class SearchBar extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        this.handleSearch = this.handleSearch.bind(this)
    }

    handleSearch = function() {
        console.log("Search")
        fetch('/search')
            .then(res => res.json())
            .then(groups => this.setState({groupsList: groups}, () => console.log('Data fetched', groups)))
    }

    render() {
        return(
            <div id="searchBar">
                <form action="/search" method="GET" className="form-inline">
                    <div className="form-group">
                        <input type="text" name="searchInput" placeholder="Suche..." className="form-control" />
                        {/*<input type="submit" value="Search" className="btn btn-default" />
                        <img id='searchIcon' src={searchIcon} alt='Search' onClick={this.handleSearch}/>*/}
                    </div>
                </form>

            </div>
        )
    }
}

export default SearchBar;