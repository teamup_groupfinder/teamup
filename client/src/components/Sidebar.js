/**
 * @author Marcel Singer/aMMokschaf
 * @author Jonathan Krueger
 */

/**
 * Sidebar shows the current user's profile as an icon above his or her groups.
 * Links to groups are clickable to be displayed in Content-Component.
 * 
 * TODO: Link-list
 */

import React, { Component } from 'react'

import Person from './Person'
import '../css/Sidebar.css'
/*import userIcon from '../images/genericUserIcon.png'*/
import userIcon from '../images/harry_icon.png'

class Sidebar extends Component {
/*	constructor(props) {
		super(props)
	}
*/
	render() {
		
		// Zauberei fuer die Praesentation
		var harryIcon
		if(this.props.currentUser.personID === 123){
			harryIcon = userIcon
		}
		return (
			<div className="sidebar">
				<div id='userIcon'>
					<img src={harryIcon} id='userIconImage' alt='' />
				</div>
				<div id="currentUser">
					<Person icon={true} personID={this.props.currentUser.personID} firstName={this.props.currentUser.firstName} lastName={this.props.currentUser.lastName} />
				</div>
				<div id="userGroups">
					<ol>	
						<li onClick={this.props.handleShowGroup}>Group<br/></li>
						<li onClick={this.props.handleShowGroup}>Group<br/></li>
						<li onClick={this.props.handleShowGroup}>Group<br/></li>
					</ol>
				</div>
			</div>
		)
	}
}

export default Sidebar;