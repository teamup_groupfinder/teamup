/**
 * @author Marcel Singer/aMMokschaf
 */

const dbName = 'teamup'

module.exports = {
    mongoURI: 'mongodb+srv://teamup_server:teamup_server@teamup-lkymy.mongodb.net/' + dbName + '?retryWrites=true&w=majority'
}