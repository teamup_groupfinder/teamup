/**
 * @author Marc Weber
 * @author Marcel Singer/aMMokschaf
 */

var mongoose = require('mongoose')
const Schema = mongoose.Schema

const FeedbackSchema = new Schema({
    text: {
        type: Array,
        required: false
    }
})

var feedback
try {
    feedback = mongoose.model('feedback')
} catch (error) {
    feedback = mongoose.model('feedback', FeedbackSchema)
}

module.exports = Feedback = feedback