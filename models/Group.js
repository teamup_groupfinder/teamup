/**
 * @author Marcel Singer/aMMokschaf
 */

var mongoose = require('mongoose')
const Schema = mongoose.Schema

const GroupSchema = new Schema({
    groupName: {
        type: String,
        required: false
    },
    groupID: {
        type: Number,
        required: false
    },
    memberIDs: {
        type: Array,
        required: false
    },
    subjectID: {
        type: Number,
        required: false
    },
    maxMembers: {
        type: Number,
        required: false
    },
    numberOfMembers: {
        type: Number,
        required: false
    },
    description: {
        type: String,
        required: false
    },
    instructor: {
        type: Number,
        required: false
    },
    creationDate: {
        type: Number,
        required: false
    },
    alreadyTried: {
        type: Boolean,
        required: false
    }
})

var groupModel
try {
    groupModel = mongoose.model('group')
} catch (error) {
    groupModel = mongoose.model('group', GroupSchema)
}

module.exports = Group = groupModel