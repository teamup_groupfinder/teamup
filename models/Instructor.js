/**
 * @author Marcel Singer/aMMokschaf
 */

var mongoose = require('mongoose')
const Schema = mongoose.Schema

const InstructorSchema = new Schema({
    instructorFirstName: {
        type: String,
        required: false
    },
    instructorLastName: {
        type: String,
        required: false
    },
    instructorID: {
        type: Number,
        required: false
    },
    subjectIDs: {
        type: [],
        required: false
    }
})

var instructorModel
try {
    instructorModel = mongoose.model('instructor')
} catch (error) {
    instructorModel = mongoose.model('instructor', InstructorSchema)
}

module.exports = Instructor = instructorModel