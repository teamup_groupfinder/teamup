/**
 * @author Marcel Singer/aMMokschaf
 */

var mongoose = require('mongoose')
const Schema = mongoose.Schema

const PersonSchema = new Schema({
    firstName: {
        type: String,
        required: false
    },
    lastName: {
        type: String,
        required: false
    },
    personID: {
        type: Number, //Change to String to get groupmembers working, change to Number to get api working
        required: false
    },
    matNumber: {
        type: Number,
        required: false
    },
    mail: {
        type: String,
        required: false
    },
    course: {
        type: String,
        required: false
    },
    lookingForGroup: {
        type: Boolean,
        required: false
    }
})

var personModel
try {
    personModel = mongoose.model('person')
} catch (error) {
    personModel = mongoose.model('person', PersonSchema)
}

module.exports = Person = personModel