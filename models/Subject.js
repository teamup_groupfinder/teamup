/**
 * @author Marcel Singer/aMMokschaf
 */

var mongoose = require('mongoose')
const Schema = mongoose.Schema

const SubjectSchema = new Schema({
    subjectName: {
        type: String,
        required: false
    },
    subjectID: {
        type: Number,
        required: false
    },
    subjectDescription: {
        type: String,
        required: false
    },
    instructorID: {
        type: Number,
        required: false
    },
    official: {
        type: Boolean,
        required: false
    }
})

var subjectModel
try {
    subjectModel = mongoose.model('subject')
} catch (error) {
    subjectModel = mongoose.model('subject', SubjectSchema)
}

module.exports = Subject = subjectModel