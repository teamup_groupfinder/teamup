/**
 * @author Marcel Singer/aMMokschaf
 * @author Marc Weber
 */

const express = require('express')
const router = express.Router({strict: true})

const Group = require('../models/Group')
const Feedback = require('../models/Feedback')

// Datenbank Utility Klasse einbinden
const DB_Util = require('../DB_Util')

// Datenbank Utility Klasse instanziieren
const db_util = new DB_Util

router.get('/:personID/:subject', (req, res, test) => {
    // console.log('DASHBUTTON PRESSED. GET READY FOR THE ULTIMATE GROUP-FINDING EXPERIENCE OF DOOM OF DESTRUCTION')
    // req.params.subject sent by frontend:
    // Mathematik 1: 1
    // Mathematik 2: 2
    // Mathematik 3: 3
    // Software-Praktikum: 4
    // WT2: 5
    // console.log('Passed userID and subject:', req.params.personID, req.params.subject)
        
    function getFeedback(f){
        var feedbackID
    
        Feedback.create({
            text: f
        })
        .then((feedback) => {
            // console.log('FEEDBACK ID: ', feedback._id)
            feedbackID = feedback._id
            Feedback.findOne({_id: feedback._id, function(err, obj) {console.log(obj)}})
            .then((feedback) => {
                // console.log(feedback)
                res.json(feedback)
            })        
            .then(()=>{
                Feedback.findById(feedbackID)
                .then(feedback => feedback.remove())
            })
        })
    }

    getMeAGroupNowWithRanking(Number(req.params.personID), Number(req.params.subject), getFeedback)

    // SERVER_UTIL: Beispiele 
    // ------------------------------------------------------------------

    // Backup in Pfad und Datei

    // db_util.downloadAllGroups('./backup', 'backup.json')
    // db_util.uploadAllGroups('./backup', 'backup.json')

    // Gruppe per ID finden und eine MemberID entfernen: ACHTUNG! Code muss hier in die Callback-Funktion sonst Sync-Probleme

    // db_util.findGroupByID(8851, (group) => {    
    // 	group.memberIDs = group.memberIDs.filter(id => id != '666')
    // 	db_util.updateGroupByID(8851 , 'memberIDs' , group.memberIDs)
    // })

    // Gruppen-Attribut aendern/updaten mit 'name-value-pair'

    // var name = 'groupName'
    // var value = 'Lerngruppe: Statik'
    // db_util.updateGroupByID(4412, name, value)

    // ------------------------------------------------------------------
})

var alreadyTriedGroups = []

function getMeAGroupNowWithRanking(userID, subjectID, callback) {
    db_util.getAllGroups(groups => {
        //1. Fetch all groups
        //TODO: Check if there are 0 groups, if not: proceed
        var allGroups = groups
        // console.log('All fetched groups:', allGroups)
        var groupsWithSubject = allGroups.filter((group) => {
            var i
            console.log(group.alreadyTried)
            for(i = 0; i < allGroups.length; i++) {
                if(group.subjectID === subjectID) {
                    return group
                }
            }
            return null
        })
        console.log(alreadyTriedGroups)

        //Remove all groups that were already tried
        if(!(alreadyTriedGroups.length === 0)) {
            var groupsWithSubject = groupsWithSubject.filter((group) => {
                var j
                for(i = 0; i < groupsWithSubject.length; i++) {
                    for(j = 0; j < alreadyTriedGroups.length; j++) {
                        if(!(group.groupID === alreadyTriedGroups[j])) {
                            return group
                        }
                    }
                }
                return null
            })
        }
        console.log(groupsWithSubject)
        //TODO: Check if there are 0 groups, if not: proceed
        
        //2. Filter groups by requested subjectID
        // console.log('All groups with requested subject', groupsWithSubject)
        var i
        var groupsScoresReasons = []
        for(i = 0; i < groupsWithSubject.length; i++) {
            groupsScoresReasons.push([groupsWithSubject[i], 0, []])
        }

        //3. Remove groups the user is already in
        //TODO: 3.
        //TODO: Check if there are 0 groups, if not: proceed

        //4. Give every group a score of 0 and an empty array for ranking-reasons
        // console.log('Groups have score = 0', groupsScoresReasons)		
        for(i = 0; i < groupsScoresReasons.length; i++) {
            //Ranking:
            
            //First item in reasons-Array is groupName
            groupsScoresReasons[i][2].push(groupsScoresReasons[i][0].groupName)

            //Full groups get score -10
            if(groupsScoresReasons[i][0].numberOfMembers === groupsScoresReasons[i][0].maxMembers) {
                groupsScoresReasons[i][1] -= 10
                groupsScoresReasons[i][2].push("Diese Gruppe ist voll.")
            }

            //TODO: Implement group-member-rating
            //Mean-rating of group-members
            //		- > 4: +1
            //		- > 3: +0.5
            //		- < 3: 0
            //		- < 2: -1
            //		- < 1: -2

            //If there is only a single place left to complete the group, ranking++
            if(groupsScoresReasons[i][0].numberOfMembers === groupsScoresReasons[i][0].maxMembers - 1) {
                groupsScoresReasons[i][1]++
                groupsScoresReasons[i][2].push("Diese Gruppe wurde durch deinen Beitritt komplettiert.") 
            }
        }

        //Oldest Group gets score +1
        groupsScoresReasons.sort(sortByAge)
        // console.log('Sorted by Age: ', groupsScoresReasons)
        groupsScoresReasons[0][1]++
        groupsScoresReasons[0][2].push("Diese Gruppe besteht bereits am längsten.") 

        // console.log('Score complete, requires sorting:', groupsScoresReasons)
        groupsScoresReasons.sort(sortByScore)

        // console.log('Sorting complete. Sorted groups:', groupsScoresReasons)
        //The best group is the last in the array
        var bestGroup = groupsScoresReasons[groupsScoresReasons.length - 1]
        // console.log('Best group: ', bestGroup)

        // Add user to group in db via update-method, update number of members-property
        var members = bestGroup[0].memberIDs
        members.push(String(userID))

        alreadyTriedGroups.push(bestGroup[0].groupID)

        db_util.updateGroupByID(bestGroup[0].groupID, "memberIDs", members)
        db_util.updateGroupByID(bestGroup[0].groupID, "numberOfMembers", members.length)

        //TODO: res.send score and reasons back to frontend
        //console.log('Reasons: ', bestGroup[2])
        callback(bestGroup[2])        
    })
}

//Sort-functions, can be passed to array.sort()
function sortByAge(a, b) {
    var colIndex = 0
    if (a[colIndex].creationDate === b[colIndex].creationDate) {
        return 0
    }
    else {
        return (a[colIndex].creationDate < b[colIndex].creationDate) ? -1 : 1
    }
}

function sortByScore(a, b) {
    var colIndex = 1
    if (a[colIndex] === b[colIndex]) {
        return 0
    }
    else {
        return (a[colIndex] < b[colIndex]) ? -1 : 1
    }
}

//TODO:
//Pseudocode for some teamup-methods, what do you think?
/*
findGroupForAllRandomly() {
	get all groups for that subject
	get all persons that are interested
	create array: groupID (eg: 1001 - 1010), empty Array for MemberIDs
	for every member: Generate random number from 1-10, add member to group
	return array of groups
}
*/

/*
getMeAGroupNow(subjectID) {
	get all groups for that subject
	generate score?
	return groupID:int, reasonsForThisGroup:String[]
}
*/

/*
findGroupAtCertainDate(subjectID, official: true/false) {
	get all groups for that subject that are official true/false
	get all persons that are interested
	create array: groupID, score=0

	group is not full: 						+1
	group is only missing one member		+1
	groupmembers mean-rating is 4+ stars: 	+1
	group is the oldest (FIFO)				+1
	
	sort array by score
	return array[0]'s groupID, reasonsForThisGroup:String[]
}
*/

module.exports = router