/**
 * @author: Nils 'ng0' Gillmann
 * @author: Marcel Singer/aMMokschaf
 * @author: Marc W.
 */

const express = require('express')
const router = express.Router()

//Group Model
const Group = require('../models/Group')

// @route GET groups
// @desc Get all Groups
// @access Public

// Slash means, we are in routes-folder
router.get('/', (req, res) => {
    Group.find()
        //.sort({ date: -1 })
        .then(groups => res.json(groups))
})

// Get one Group by ID, e.g: localhost:5000/groups/7852
router.get('/:groupID', (req, res) => {
    console.log("Getting single group")
    Group.findOne({groupID: req.params.groupID, function(err, obj) {console.log(obj)}})
        .then((group) => res.json(group))
})

// @route POST groups
// @desc Create A Post
// @access Public

// Slash means, we are in routes-folder
router.post('/', (req, res) => {
    const newGroup = new Group({
        groupName: req.body.groupName,
        subjectID: req.body.subjectID,
        groupID: req.body.groupID,
        memberIDs: req.body.memberIDs,
        maxMembers: req.body.maxMembers,
        numberOfMembers: 0,
        description: req.body.description,
        instructor: req.body.instructor
    })
    newGroup.save().then(group => res.json(group))
})

// @route Delete groups
// @desc Delete a group
// @access Public

// Slash means, we are in routes-folder
router.delete('/:_id/delete', (req, res) => {
    Group.findById(req.params._id)
    .then(group => group.remove().then(() => res.json(group)))
    .catch(err => res.status(404).json({ success: false }))
})

// update group
router.post('/:_id/update', (req, res) => {
    console.log('update-method', req.params, req.body)
	Group.findByIdAndUpdate(req.params._id, {$set: req.body}, {useFindAndModify: false},
		function(err, obj) {
		    if(err) {
                return next(err);
            }
			res.send('Group updated')
        }
    )
})

module.exports = router
