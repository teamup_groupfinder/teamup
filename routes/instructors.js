/**
 * @author: Marcel Singer/aMMokschaf
 * @author: Nils 'ng0' Gillmann
 */

const express = require('express')
const router = express.Router()

//Instructor Model
const Instructor = require('../models/Instructor')

// @route GET Instructors
// @desc Get all Instructors
// @access Public

// Slash means, we are in routes-folder
router.get('/', (req, res) => {
    Instructor.find()
        //.sort({ date: -1 })
        .then(instructors => res.json(instructors))
})

// Get one Instructor by ID, e.g: localhost:5000/instructors/1003
router.get('/:instructorID', (req, res) => {
    console.log("Getting single instructor")
    Instructor.findOne({instructorID: req.params.instructorID, function(err, obj) {console.log(obj)}})
        .then((instructor) => res.json(instructor))
})

// @route POST instructors
// @desc Create A Post
// @access Public

// Slash means, we are in routes-folder
router.post('/', (req, res) => {
    const newInstructor = new Instructor({
        instructorFirstName: req.body.instructorFirstName,
        instructorLastName: req.body.instructorLastName,
        instructorID: req.body.instructorID,
        subjectIDs: req.body.subjectIDs,
    })
    newInstructor.save().then(instructor => res.json(instructor))
})

// @route Delete instructor
// @desc Delete a instructor
// @access Public

// Slash means, we are in routes-folder
router.delete('/:id', (req, res) => {
    Instructor.findById(req.params.instructorID)
    .then(instructor => instructor.remove().then(() => res.json(instructor)))
    .catch(err => res.status(404).json({ success: false }))
})

// update
router.post('/:id/update', (req, res) => {
	Instructor.findByIdAndUpdate(req.params.instructorID, {$set: req.body},
				     function (err, obj) {
					     if (err)
						     return next(err);
					     res.send('Instructor update');
				     });
});

module.exports = router
