/**
 * @author: Marcel Singer/aMMokschaf
 * @author: Nils 'ng0' Gillmann
 */

const express = require('express')
const router = express.Router()

//Person Model
const Person = require('../models/Person')

// @route GET persons
// @desc Get all persons
// @access Public

// Slash means, we are in routes-folder
router.get('/', (req, res) => {
    Person.find()
        //.sort({ date: -1 })
        .then(persons => res.json(persons))
})

// Get one Person by ID, e.g: localhost:5000/persons/7521
router.get('/:personID', (req, res) => {
    console.log("Getting single person")
    Person.findOne({personID: req.params.personID, function(err, obj) {console.log(obj)}})
        .then((person) => res.json(person))
})

// @route POST persons
// @desc Create A Post
// @access Public

// Slash means, we are in routes-folder
router.post('/', (req, res) => {
    const newPerson = new Person({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        personID: req.body.personID,
        matNumber: req.body.matNumber,
        mail: req.body.mail,
        course: req.body.course,
        lookingForGroup: req.body.true,
    })
    newPerson.save().then(person => res.json(person))
})

// @route Delete persons
// @desc Delete a person
// @access Public

// Slash means, we are in routes-folder
router.delete('/:personID', (req, res) => {
    Person.findById(req.params.personID)
    .then(person => person.remove().then(() => res.json(person)))
    .catch(err => res.status(404).json({ success: false }))
})

router.post('/:personID/update', (req, res) => {
	Person.findByIdAndUpdate(req.params.personID, {$set: req.body},
		function (err, obj) {
			if(err) {
                return next(err)
            }
			res.send('Person update')
        }
    )
})

module.exports = router
