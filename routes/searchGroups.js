/**
 * @author Marcel Singer/aMMokschaf
 */

/**
 * Dummy-component, does not work
 */

const express = require('express')
const router = express.Router()

//Group Model
const Group = require('../models/Group')

// @route GET groups
// @desc Get all Groups
// @access Public

// Slash means, we are in routes-folder
router.get('/search', (req, res) => {
    console.log("Suche")
})

module.exports = router