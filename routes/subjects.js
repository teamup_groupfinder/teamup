/**
 * @author: Marcel Singer/aMMokschaf
 * @author: Nils 'ng0' Gillmann
 */

const express = require('express')
const router = express.Router()

//Subject Model
const Subject = require('../models/Subject')

// @route GET subjects
// @desc Get all Subjects
// @access Public

// Slash means, we are in routes-folder
router.get('/', (req, res) => {
    Subject.find()
        //.sort({ date: -1 })
        .then(subjects => res.json(subjects))
})

// Get one Subject by ID, e.g: localhost:5000/subjects/0003
router.get('/:subjectID', (req, res) => {
    console.log("Getting single subject")
    Subject.findOne({subjectID: req.params.subjectID, function(err, obj) {console.log(obj)}})
        .then((subject) => res.json(subject))
})

// @route POST subjects
// @desc Create A Post
// @access Public

// Slash means, we are in routes-folder
router.post('/', (req, res) => {
    const newSubject = new Subject({
        subjectName: req.body.subjectName,
        subjectID: req.body.subjectID,
        subjectDescription: req.body.subjectDescription,
        instructorID: req.body.instructorID,
        official: req.body.official
    })
    newSubject.save().then(subject => res.json(subject))
})

// @route Delete subjects
// @desc Delete a subject
// @access Public

// Slash means, we are in routes-folder
router.delete('/:id', (req, res) => {
    Subject.findById(req.params.subjectID)
    .then(subject => subject.remove().then(() => res.json(subject)))
    .catch(err => res.status(404).json({ success: false }))
})

router.post('/:id/update', (req, res) => {
	Subject.findByIdAndUpdate(req.params.subjectID, {$set: req.body},
				  function (err, obj) {
					  if (err)
						  return next(err);
					  res.send('Subject updated');
				  });
});

module.exports = router
