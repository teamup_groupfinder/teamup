/**
 * @author Marcel Singer/aMMokschaf
 * @author Marc Weber
 * @author Nils 'ng0' Gillmann
 */

var express = require('express')
var mongoose = require('mongoose')
//var path = require('path')
//var Group = require('./models/Group.js')
var bodyParser = require('body-parser')

// Datenbank Utility Klasse einbinden
const DB_Util = require('./DB_Util')

const groups = require('./routes/groups')
const persons = require('./routes/persons')
const searchGroups = require('./routes/searchGroups')
const instructors = require('./routes/instructors')
const subjects = require('./routes/subjects')
const dashButton = require('./routes/dashButton')

const app = express();

// Datenbank Utility Klasse instanzieren
const db_util = new DB_Util

app.use(bodyParser.json())

//DB Config: User, password, connectString
const db = require('./config/keys').mongoURI;

//Connect to MongoDB
mongoose
  .connect(db, {useNewUrlParser: true})
  .then(() => console.log('Connection to MongoDB established.'))
  .catch(err => console.log(err))

//Use Routes
app.use('/groups', groups)
app.use('/persons', persons)
app.use('/search', searchGroups)
app.use('/instructors', instructors)
app.use('/subjects', subjects)
app.use('/dashButton', dashButton)
app.use('/deleteAll', ()=>{
  console.log('delete DB')
  db_util.deleteAllGroups()
  db_util.deleteAllPersons()  
})
app.use('/backup', ()=>{
  console.log('backup server')  
  db_util.uploadAllGroups('./backup', 'backupGroups.json')
  db_util.uploadAllPersons('./backup', 'backupPersons.json')
})

const port = process.env.PORT || 5000;

app.listen(port, () => `Server running on port ${port}`);


// SERVER_UTIL: Beispiele 
// ------------------------------------------------------------------

// Backup in Pfad und Datei

// db_util.uploadAllGroups('./backup', 'backupGroups.json')
// db_util.downloadAllGroups('./backup', 'backupGroups.json')
// db_util.uploadAllPersons('./backup', 'backupPersons.json')
// db_util.downloadAllPersons('./backup', 'backupPersons.json')

// Gruppe per ID finden und eine MemberID entfernen: ACHTUNG! Code muss hier in die Callback-Funktion sonst Sync-Probleme

// db_util.findGroupByID(8851, (group) => {    
// 	group.memberIDs = group.memberIDs.filter(id => id != '666')
// 	db_util.updateGroupByID(8851 , 'memberIDs' , group.memberIDs)
// })

// Gruppen-Attribut aendern/updaten mit 'name-value-pair'

// var name = 'groupName'
// var value = 'Lerngruppe: Statik'
// db_util.updateGroupByID(4412, name, value)

// ------------------------------------------------------------------
